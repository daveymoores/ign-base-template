// Foundation JavaScript
// Documentation can be found at: http://foundation.zurb.com/docs
$(document).foundation();

$(document).ready(function(){

	if($('#no_age_cookie').length) {
		$('#no_age_cookie').appendTo('#start_modal').css('visibility', 'visible');
		$('#start_modal').foundation('reveal', 'open');
		$('.chosen-container').css('width', '31.5%');
	}


	new Share(".share_btn", {
		ui: {
		  flyout: "bottom center" 
		},
	  networks: {
	    facebook: {
	      app_id: "abc123"
	    }
	  }
	});
});
